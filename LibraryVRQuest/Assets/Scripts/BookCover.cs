﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookCover : MonoBehaviour
{
    private static int queue = 0;
    private RenderTexture rt;
    void Start()
    {
        queue++;
        rt = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
        rt.Create();
        GetComponent<Renderer>().material.mainTexture = rt;
        StartCoroutine(ChangeTextureRender(rt));
    }

    IEnumerator ChangeTextureRender(RenderTexture rt)
    {
        int _queue = queue;
        for (int i = 0; i < _queue; i++)
        {
            yield return null;
        }
        GameObject.Find("Camera Render").GetComponent<Camera>().targetTexture = rt;
        //yield return new WaitForSeconds(queue / 50 + 1f);
    }
}
