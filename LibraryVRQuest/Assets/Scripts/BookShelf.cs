﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookShelf {

    private GameObject bookShelf;
    private int bookShelfID;
    private int nbShelves;
    private Library.Position position;
    private Library librarySettings;
    private const float shelfHeight = 2.75f/4;

    public static int bookShelfCount = 0;
    public static List<BookShelf> list = new List<BookShelf>();

  
    public BookShelf(int bookShelfNumber, int shelfQuantity, Library.Position _position, GameObject row)
    {
        bookShelfID = bookShelfNumber;
        nbShelves = shelfQuantity;
        position = _position;
        
        bookShelf = new GameObject("BookShelf " + bookShelfID);
        bookShelf.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        bookShelf.transform.SetParent(row.transform);
        bookShelfCount++;
        
        librarySettings = GameObject.Find("LibraryManager").GetComponent<Library>();
        list.Add(this);
        PopulateBookShelf();
        
        //DebugPrint();
    }
  
    
    private void PopulateBookShelf()
    {
        int booksPerShelves = librarySettings.booksPerShelf;
       
        for (int i = 0; i < nbShelves; i++)
        {
            new Shelf(i + bookShelfID * nbShelves, booksPerShelves, new Library.Position(position.posX, i * shelfHeight, position.posZ), bookShelf);
        }
    }
    
    
    
    
    public Library.Position GetPosition()
    {
        return position;
    }

    public GameObject GetGameObject()
    {
        return bookShelf;
    }

    private void DebugPrint()
    {
        Debug.Log("BS n#" + bookShelfID + " position: " + position.posX + ";" + position.posY + ";" + position.posZ + "\n");
    }
}
