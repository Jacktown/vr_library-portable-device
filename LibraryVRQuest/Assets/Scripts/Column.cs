﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column
{

    private GameObject column;
    private int columnID;
    private int nbRows;
    private Library.Position position;
    private Library librarySettings;

    public static int columnCount = 0;

    public Column(int columnNumber, int rowQuantity, Library.Position _position,
        GameObject library)
    {
        columnID = columnNumber;
        nbRows = rowQuantity;
        position = _position;

        column = new GameObject("Column " + columnID);
        column.transform.position = new Vector3(position.posX, position.posY, position.posZ);
        column.transform.SetParent(library.transform);
        
        columnCount++;
        librarySettings = GameObject.Find("LibraryManager").GetComponent<Library>();
        PopulateColumn(column);
        
        //DebugPrint();
    }

    
    private void PopulateColumn(GameObject parent)
    {
        for (int i = 0; i < nbRows; i++)
        {
            new Row(i + columnID * nbRows, librarySettings.sizeRow, 
                new Library.Position(position.posX, 0, position.posZ + i * Library.rowSpace), column);       
        }
    }
    
    private void DebugPrint()
    {
        Debug.Log("Column n#" + columnID + " position: " + position.posX + ";" + position.posY + ";" + position.posZ + "\n");
    }
}