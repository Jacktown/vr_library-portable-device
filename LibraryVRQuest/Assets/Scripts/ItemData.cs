﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : MonoBehaviour
{
    private String bookTitle;
    private Inventory _inventory;

    public void InitializeItem(String s)
    {
        bookTitle = s;
        _inventory = GameObject.Find("InventoryMenu").GetComponent<Inventory>();
    }

    public void ButtonActivated()
    {
        _inventory.ChangeSelector(bookTitle);
    }
}

