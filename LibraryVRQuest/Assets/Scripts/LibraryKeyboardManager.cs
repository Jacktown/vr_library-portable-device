﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LibraryKeyboardManager : MonoBehaviour
{

    public InputField inputField;
    private bool shiftState = false;
    private bool capsState = false;
    public WikiScraper wikiScraper;

    public void ToggleShift()
    {
        shiftState = !shiftState;
    }

    public void ToggleCaps()
    {
        capsState = !capsState;
    }

    public bool IsShiftPressed()
    {
        return shiftState;
    }

    public bool IsCapsLocked()
    {
        return capsState;
    }

    public void ClearInput()
    {
        inputField.text = "";
    }

    public void Backspace()
    {
        inputField.text = inputField.text.Substring(0, inputField.text.Length - 1);
    }
    

    public void SearchBook(bool reset)
    {
        wikiScraper.Research(reset);
    }

}
