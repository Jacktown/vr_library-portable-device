﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class bookEventHandler : MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler,
    IPointerClickHandler
{
    public Inventory inventory;
    //public Text debug;

    private BookData _bookData;

    private void Start()
    {
        _bookData = GetComponent<BookData>();
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.IsViveButton(ControllerButton.Trigger))
        {
            inventory.AddToInventory(_bookData);
            inventory.SaveGameObject(_bookData.GetTitle(), gameObject);
        }
    }
}
